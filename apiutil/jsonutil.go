package apiutil

import (
	"bitbucket.org/tsiemens/bridgescoreserver/model"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
)

func WriteJsonToResponse(w http.ResponseWriter, code int, o interface{}) error {
	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.Encode(o)
	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	w.WriteHeader(code)
	_, err := io.Copy(w, &buf)
	if err != nil {
		fmt.Println(err.Error())
	}
	return err
}

func WriteJson(writer io.Writer, o interface{}) error {
	enc := json.NewEncoder(writer)
	return enc.Encode(o)
}

func WriteJsonError(w http.ResponseWriter, code int, msg string) error {
	fmt.Printf("%d error: %s\n", code, msg)
	return WriteJsonToResponse(w, code, model.JsonErrorContainer{Error: msg})
}
