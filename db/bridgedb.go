package db

import (
	"bitbucket.org/tsiemens/bridgescoreserver/model"

	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"io/ioutil"
	"log"
	"strings"
	"time"
)

const (
	// Format is supposed to be for Mon Jan 2 15:04:05 -0700 MST 2006
	datetimeForm = "2006-01-02 15:04:05"
)

func ParseDatetime(datetime string) (time.Time, error) {
	return time.Parse(datetimeForm, datetime)
}

func OpenMysql(db string, user string, pass string) (*sql.DB, error) {
	return sql.Open("mysql", fmt.Sprintf("%s:%s@/%s", user, pass, db))
}

// Read filename, which should be formatted like so (in any order):
// database=<dbname>
// user=<user>
// password=<password>
// Then returns a sql.DB opened for the MySQL db.
func OpenMysqlFromConfigFile(filename string) (*sql.DB, error) {
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	datStr := string(dat)
	lines := strings.Split(datStr, "\n")

	var db *string
	var user *string
	var pass *string
	for _, line := range lines {
		parts := strings.Split(line, "=")
		if len(parts) == 2 {
			if parts[0] == "database" {
				db = &parts[1]
			} else if parts[0] == "user" {
				user = &parts[1]
			} else if parts[0] == "password" {
				pass = &parts[1]
			}
		}
		// Otherwise, ignoring this line
	}
	if db == nil || user == nil || pass == nil {
		return nil, fmt.Errorf("Not all config fields filled")
	}
	return OpenMysql(*db, *user, *pass)
}

type Database interface {
	Close()
	GetSessions() ([]model.Session, error)
	GetSession(model.SessionId) (model.Session, bool, error)
	AddSession(*model.Session) error
	DelSession(model.SessionId) error

	GetSessionBoardSets(model.SessionId) ([]model.BoardSet, error)
	AddOrUpdateBoardSet(*model.BoardSet, model.SessionId) error
	// DelBoardSet(model.PublisherId, model.SessionId) error
}

type MysqlDb struct {
	conn *sql.DB
}

// Compile time assertion for interface implementation
var _ Database = (*MysqlDb)(nil)

func NewMysqlDb(db *sql.DB) *MysqlDb {
	return &MysqlDb{conn: db}
}

func (db *MysqlDb) Close() {
	db.conn.Close()
}

// Expects a row queried with id, name, timestamp
func (db *MysqlDb) sessionFromRows(
	rows *sql.Rows) (session model.Session, err error) {

	var sessId int64
	var datetime string
	err = rows.Scan(&sessId, &session.Name, &datetime)
	if err != nil {
		return
	}
	session.Id = model.SessionId(sessId)
	// datetime is not null, so it shouldn't fail
	time, err := ParseDatetime(datetime)
	if err != nil {
		return
	}
	session.Timestamp = time.Unix() * 1000
	// if passwordPtr == nil {
	// password = ""
	// } else {
	// password = *passwordPtr
	// }
	return
}

func (db *MysqlDb) GetSessions() ([]model.Session, error) {
	rows, err := db.conn.Query("SELECT id, name, timestamp FROM Session")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var sessions []model.Session
	for rows.Next() {
		session, err := db.sessionFromRows(rows)
		if err != nil {
			return nil, err
		}
		sessions = append(sessions, session)
	}
	return sessions, nil
}

func (db *MysqlDb) GetSession(sessId model.SessionId) (
	session model.Session, ok bool, err error) {

	ok = false
	rows, err := db.conn.Query("SELECT id, name, timestamp FROM Session "+
		"WHERE id=?", int64(sessId))

	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		session, err = db.sessionFromRows(rows)
		if err != nil {
			return
		}
		ok = true
	}
	return
}

func (db *MysqlDb) Transact(fp func(*sql.Tx) error) (err error) {
	tx, err := db.conn.Begin()
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()
	// Run whatever is going on during the transaction
	if err = fp(tx); err != nil {
		return
	}

	err = tx.Commit()
	return
}

func (db *MysqlDb) AddSessionInTx(tx *sql.Tx, sess *model.Session) (
	model.SessionId, error) {
	if sess.Id != 0 {
		log.Fatal("AddSession: sess.Id was not 0")
	}

	timestamp := time.Unix(sess.Timestamp/1000, 0).Format(datetimeForm)
	var password *string
	if sess.Password != "" {
		password = &sess.Password
	}
	res, err := tx.Exec("INSERT INTO Session (name, timestamp, password) "+
		"VALUES (?, ?, ?)", sess.Name, timestamp, password)
	if err != nil {
		return 0, err
	}
	lastId, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return model.SessionId(lastId), nil
}

func (db *MysqlDb) AddSession(sess *model.Session) error {
	var sessId model.SessionId
	err := db.Transact(func(tx *sql.Tx) error {
		var addErr error = nil
		sessId, addErr = db.AddSessionInTx(tx, sess)
		return addErr
	})
	if err == nil {
		sess.Id = sessId
	}
	return err
}

func (db *MysqlDb) checkRowsAffected(
	result sql.Result, funcname string, expected int64) error {

	affected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if affected != expected {
		return fmt.Errorf("%s affected %d rows. Expected %d",
			funcname, affected, expected)
	}
	return nil
}

func (db *MysqlDb) DelSessionInTx(tx *sql.Tx, sessId model.SessionId) error {
	_, err := tx.Exec("DELETE FROM Session WHERE id=?", int64(sessId))
	if err != nil {
		return err
	}
	return nil
}

func (db *MysqlDb) DelSession(sessId model.SessionId) error {
	return db.Transact(func(tx *sql.Tx) error {
		return db.DelSessionInTx(tx, sessId)
	})
}

func (db *MysqlDb) GetSetBoards(bsId int64) ([]model.Board, error) {
	rows, err := db.conn.Query("SELECT boardNumber, suit, level, "+
		"doubled, tricks, declarer, publisherSeat FROM Board "+
		"WHERE boardSetId=?", bsId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var boards []model.Board = make([]model.Board, 0)
	var b model.Board

	for rows.Next() {
		b = model.Board{}
		err = rows.Scan(&b.BoardNumber, &b.Suit, &b.Level, &b.Doubled, &b.Tricks,
			&b.Declarer, &b.PublisherSeat)
		if err != nil {
			return nil, err
		}
		boards = append(boards, b)
	}

	return boards, nil
}

func (db *MysqlDb) GetSessionBoardSets(
	sessId model.SessionId) ([]model.BoardSet, error) {

	rows, err := db.conn.Query("SELECT id, publisherId, team FROM BoardSet "+
		"WHERE sessionId=?", sessId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var boardSets []model.BoardSet
	var bs model.BoardSet
	var pubId string

	for rows.Next() {
		bs = model.BoardSet{}
		err = rows.Scan(&bs.Id, &pubId, &bs.Team)

		if err != nil {
			return nil, err
		}
		bs.PublisherId = model.PublisherId(pubId)

		boards, err := db.GetSetBoards(bs.Id)
		if err != nil {
			return nil, err
		}
		bs.Boards = boards

		boardSets = append(boardSets, bs)
	}
	return boardSets, nil
}

func (db *MysqlDb) getBoardSetId(pubId model.PublisherId, sessId model.SessionId) (
	boardSetId int64, ok bool, err error) {

	ok = false
	rows, err := db.conn.Query("SELECT id FROM BoardSet "+
		"WHERE publisherId=? AND sessionId=?", string(pubId), int64(sessId))
	if err != nil {
		return
	}

	for rows.Next() {
		if ok {
			err = fmt.Errorf("Unexpectedly found multiple BoardSets for the pubId")
			ok = false
			return
		}
		err = rows.Scan(&boardSetId)
		if err != nil {
			return
		}
		ok = true
	}
	return
}

func (db *MysqlDb) AddBoardInTx(
	tx *sql.Tx, b model.Board, boardSetId int64) error {

	_, err := tx.Exec("INSERT INTO Board (boardSetId, boardNumber, suit, level, "+
		"doubled, tricks, declarer, publisherSeat)"+
		"VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
		boardSetId, b.BoardNumber, b.Suit, b.Level, b.Doubled, b.Tricks, b.Declarer,
		b.PublisherSeat)
	return err
}

func (db *MysqlDb) DelBoardsFromSetInTx(
	tx *sql.Tx, boardSetId int64) error {

	_, err := tx.Exec("DELETE FROM Board WHERE boardSetId=?", boardSetId)
	return err
}

func (db *MysqlDb) AddOrUpdateBoardSetInTx(
	tx *sql.Tx, boardSet *model.BoardSet, sessId model.SessionId) (int64, error) {

	dbSetId, ok, err := db.getBoardSetId(boardSet.PublisherId, sessId)
	if err != nil {
		return 0, err
	} else if !ok {
		dbSetId = 0
	}

	if dbSetId == 0 {
		// Add
		res, err := tx.Exec("INSERT INTO BoardSet (sessionId, publisherId, team) "+
			"VALUES (?, ?, ?)",
			int64(sessId), string(boardSet.PublisherId), boardSet.Team)
		if err != nil {
			return 0, err
		}
		dbSetId, err = res.LastInsertId()
		if err != nil {
			return 0, err
		}
	} else {
		// Update
		_, err := tx.Exec("UPDATE BoardSet SET sessionId = ?, publisherId = ?, "+
			"team = ? WHERE id = ?",
			int64(sessId), string(boardSet.PublisherId), boardSet.Team, dbSetId)
		if err != nil {
			return 0, err
		}
		// This can be 1 or 0
		// err = db.checkRowsAffected(res, "AddOrUpdateBoardSetInTx", 1)
		// if err != nil {
		// return 0, err
		// }
	}

	err = db.DelBoardsFromSetInTx(tx, dbSetId)
	if err != nil {
		return 0, err
	}
	for _, board := range boardSet.Boards {
		err := db.AddBoardInTx(tx, board, dbSetId)
		if err != nil {
			return 0, err
		}
	}
	return dbSetId, nil
}

func (db *MysqlDb) AddOrUpdateBoardSet(
	bs *model.BoardSet, sessId model.SessionId) error {

	var bsId int64
	err := db.Transact(func(tx *sql.Tx) error {
		var addErr error = nil
		bsId, addErr = db.AddOrUpdateBoardSetInTx(tx, bs, sessId)
		return addErr
	})
	if err == nil {
		bs.Id = bsId
	}
	return err
}

func (db *MysqlDb) DelBoardSet(model.PublisherId, model.SessionId) error {
	// TODO
	log.Fatal("Unimplemented!")
	return nil
}
