package main

import (
	dbm "bitbucket.org/tsiemens/bridgescoreserver/db"
	"bitbucket.org/tsiemens/bridgescoreserver/handler"
	"bitbucket.org/tsiemens/bridgescoreserver/util"
	"flag"
	"fmt"
	"net"
	"net/http/fcgi"
	"os"
)

var port int
var passwordSalt string
var passwordSha1 string

func printUsage() {
	fmt.Fprintf(os.Stderr, `Usage: bridgescorefcgi [OPTIONS] MYSQL_CREDSFILE
`)
	flag.PrintDefaults()
}

func main() {
	flag.Usage = printUsage
	flag.StringVar(&passwordSalt, "passsalt", "", "Salt string to used to generate the "+
		"server password hash")
	flag.StringVar(&passwordSha1, "passsha1", "", "Hex string of the [salted] server password\n"+
		"This can be generated with the provided passgen script.")
	flag.IntVar(&port, "port", 9143, "Port to listen on")
	flag.Parse()

	var password *util.Password
	if passwordSha1 != "" {
		hash, err := util.HexToHash(passwordSha1)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Invalid hash: %s\n", err.Error())
			os.Exit(1)
		}
		password = &util.Password{Salt: passwordSalt, Sha1: hash}
	}

	args := flag.Args()
	if len(args) != 1 {
		fmt.Fprintf(os.Stderr, "Invalid args\n")
		flag.Usage()
		os.Exit(1)
	}
	credsFile := args[0]

	conn, err := dbm.OpenMysqlFromConfigFile(credsFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error connecting to database: %s\n", err.Error())
		os.Exit(1)
	}
	db := dbm.NewMysqlDb(conn)
	defer db.Close()

	listener, _ := net.Listen("tcp", fmt.Sprintf("127.0.0.1:%d", port))
	h := handler.NewHandler(password, db)
	fcgi.Serve(listener, h)
}
