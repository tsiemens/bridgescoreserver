package test

import (
	"bitbucket.org/tsiemens/bridgescoreserver/db"
	"bitbucket.org/tsiemens/bridgescoreserver/model"

	"fmt"
	"runtime/debug"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

const (
	sampleDate   = "2017-01-02 15:04:05"
	sampleDateTs = 1483369445000
)

func SetupMockDb(t *testing.T) (*db.MysqlDb, sqlmock.Sqlmock) {
	conn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database "+
			"connection", err)
	}
	return db.NewMysqlDb(conn), mock
}

func CheckExpectations(t *testing.T, mock sqlmock.Sqlmock) {
	// we make sure that all expectations were met
	if err := mock.ExpectationsWereMet(); err != nil {
		debug.PrintStack()
		t.Errorf("there were unfulfilled expections: %s", err)
	}
}

func TestGetDbSessions(t *testing.T) {
	db, mock := SetupMockDb(t)
	defer db.Close()

	rows := sqlmock.NewRows([]string{"id", "name", "timestamp"}).
		AddRow(1, "Sess 1", sampleDate)

	mock.ExpectQuery("^SELECT id, name, timestamp FROM Session").
		WillReturnRows(rows)

	sessions, err := db.GetSessions()
	AssertNil(t, err)
	AssertDeepEqual(t, sessions, []model.Session{
		model.NewSession(1, "Sess 1", sampleDateTs, ""),
	})

	CheckExpectations(t, mock)
}

func TestAddDbSession(t *testing.T) {
	db, mock := SetupMockDb(t)
	defer db.Close()

	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO Session").WithArgs("Sess 1", sampleDate, nil).
		WillReturnResult(sqlmock.NewResult(100, 1))
	mock.ExpectCommit()

	sess := model.NewSession(0, "Sess 1", sampleDateTs, "")
	err := db.AddSession(&sess)
	AssertNil(t, err)
	AssertIntsEqual(t, int(sess.Id), 100)

	// Test exec failure
	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO Session").WithArgs("Sess 1", sampleDate, nil).
		WillReturnError(fmt.Errorf("Some error 1"))
	mock.ExpectRollback()

	sess = model.NewSession(0, "Sess 1", sampleDateTs, "")
	err = db.AddSession(&sess)
	AssertDeepEqual(t, err, fmt.Errorf("Some error 1"))
	AssertIntsEqual(t, int(sess.Id), 0)

	// Test commit failure
	mock.ExpectBegin()
	mock.ExpectExec("INSERT INTO Session").WithArgs("Sess 1", sampleDate, nil).
		WillReturnResult(sqlmock.NewResult(100, 1))
	mock.ExpectCommit().WillReturnError(fmt.Errorf("Some error 2"))

	sess = model.NewSession(0, "Sess 1", sampleDateTs, "")
	err = db.AddSession(&sess)
	AssertDeepEqual(t, err, fmt.Errorf("Some error 2"))
	AssertIntsEqual(t, int(sess.Id), 0)

	CheckExpectations(t, mock)
}

func TestDelDbSession(t *testing.T) {
	db, mock := SetupMockDb(t)
	defer db.Close()

	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM Session").WithArgs(20).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	err := db.DelSession(model.SessionId(20))
	AssertNil(t, err)
	CheckExpectations(t, mock)

	// Test delete already deleted
	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM Session").WithArgs(20).
		WillReturnResult(sqlmock.NewResult(0, 0)) // No rows deleted
	mock.ExpectCommit() // Idempotent

	err = db.DelSession(model.SessionId(20))
	AssertNil(t, err)
	CheckExpectations(t, mock)

	// Test db error
	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM Session").WithArgs(20).
		WillReturnError(fmt.Errorf("some error"))
	mock.ExpectRollback()

	err = db.DelSession(model.SessionId(20))
	AssertNotNil(t, err)

	CheckExpectations(t, mock)
}
