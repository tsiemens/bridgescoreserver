#!/usr/bin/env bash
function printUsage {
   echo "Usage: db_init.sh ROOT_CREDS DB_CREDS

ROOT_CREDS - a file containing the mysql root's password

DB_CREDS - a file containing the mysql db's credentials in for format:
database=<dbname>
user=<username>
password=<password>

This user will be created."
}

ROOTCREDS=$1
DBCREDS=$2

if [[ -z $ROOTCREDS ]] || [[ -z $DBCREDS ]]; then
   printUsage
   exit 1
fi

DBNAME=`grep database= $DBCREDS | sed 's/database=//'`
USER=`grep user= $DBCREDS | sed 's/user=//'`
PASS=`grep password= $DBCREDS | sed 's/password=//'`

if [[ -z $DBNAME ]] || [[ -z $USER ]] || [[ -z $PASS ]]; then
   echo "Bad db credentials file"
   exit 1
fi

mysql -u root --password="`cat $ROOTCREDS`" -e "CREATE DATABASE $DBNAME;
CREATE USER '$USER'@'localhost' IDENTIFIED BY '$PASS';
GRANT ALL PRIVILEGES ON $DBNAME.* TO '$USER'@'localhost';
FLUSH PRIVILEGES;" && echo "Done"
