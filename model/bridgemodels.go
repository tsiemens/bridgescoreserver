package model

import (
	"bitbucket.org/tsiemens/bridgescoreserver/util"
	"fmt"
)

const None = "none"

const (
	NoTrumps = "noTrumps"
	Spades   = "spades"
	Hearts   = "hearts"
	Diamonds = "diamonds"
	Clubs    = "clubs"
)

var Suits = map[string]int{
	None:     0,
	NoTrumps: 1,
	Spades:   2,
	Hearts:   3,
	Diamonds: 4,
	Clubs:    5,
}

const (
	Undoubled = "undoubled"
	Doubled   = "doubled"
	Redoubled = "redoubled"
)

var DoubledState = map[string]int{
	Undoubled: 0,
	Doubled:   1,
	Redoubled: 2,
}

const (
	North = "north"
	East  = "east"
	South = "south"
	West  = "west"
)

var Direction = map[string]int{
	None:  0,
	North: 1,
	East:  2,
	South: 3,
	West:  4,
}

type PublisherId string

func (id *PublisherId) PrivateToPublic() PublisherId {
	return PublisherId(util.Sha1HashStringToString(string(*id)))
}

type SessionId int64

type Board struct {
	BoardNumber   int
	Suit          string
	Level         int
	Doubled       string
	Tricks        int
	Declarer      string
	PublisherSeat string
}

func (b *Board) IsValid() (bool, error) {
	if b.BoardNumber < 1 || b.BoardNumber > 99 {
		return false, fmt.Errorf("Invalid BoardNumber: %d", b.BoardNumber)
	}
	if _, ok := Direction[b.PublisherSeat]; !ok || b.PublisherSeat == None {
		return false, fmt.Errorf("Invalid PublisherSeat: %s", b.PublisherSeat)
	}
	if b.Level < 0 || b.Level > 7 {
		return false, fmt.Errorf("Invalid Level: %d", b.Level)
	}
	// Handle no bid validity
	if b.Level == 0 {
		if b.Suit != None || b.Doubled != Undoubled || b.Tricks != 0 ||
			b.Declarer != None {
			return false, fmt.Errorf("Invalid No-bid board: %v", b)
		}
		return true, nil
	}

	if _, ok := Suits[b.Suit]; !ok || b.Suit == None {
		return false, fmt.Errorf("Invalid Suit: %s", b.Suit)
	}
	if _, ok := DoubledState[b.Doubled]; !ok {
		return false, fmt.Errorf("Invalid Doubled: %s", b.Doubled)
	}
	if b.Tricks < 0 || b.Tricks > 13 {
		return false, fmt.Errorf("Invalid Tricks: %d", b.Tricks)
	}
	if _, ok := Direction[b.Declarer]; !ok || b.Declarer == None {
		return false, fmt.Errorf("Invalid Declarer: %s", b.Declarer)
	}

	return true, nil
}

type BoardSet struct {
	Id          int64 // Database use only
	Team        string
	PublisherId PublisherId
	Boards      []Board
}

type SessionBase struct {
	Id        SessionId
	Name      string
	Timestamp int64 // Unix milliseconds
}

type Session struct {
	SessionBase
	Password string
}

func NewSession(id SessionId, name string, timestamp int64, password string) Session {
	return Session{
		SessionBase: SessionBase{Id: id, Name: name, Timestamp: timestamp},
		Password:    password,
	}
}
